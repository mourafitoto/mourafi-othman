﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjSVP.Models
{
    public class Restaurant
    {
        [Key]
        public String restaurantID { get; set; }
        public String nomrest { get; set; }
        public String adresserest { get; set; }
        public virtual ICollection<Table> tables { get; set; }
    }
}