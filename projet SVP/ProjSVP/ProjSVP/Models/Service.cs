﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjSVP.Models
{
    public class Service
    {
        public String tableService { get; set; }
        public String clientService { get; set; }
        public DateTime date { get; set; }
        enum TypeService
        {
            Serveur,
            Addition,
            Annuler,
        }
        [ForeignKey("tableService")]
        public Table table { get; set; }
        [ForeignKey("clientService")]
        public Client client { get; set; }
    }
}
