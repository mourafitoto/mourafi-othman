﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjSVP.Models
{
    public class Table
    {
        [Key]
        public String tableID { get; set; }
        public uint numeroTable { get; set; }
        public uint capacite { get; set; }
        public String etat { get; set; }
        public String restaurantID { get; set; }


        [ForeignKey("RestaurantID")]
        public Restaurant restaurant { get; set; }
    }
}