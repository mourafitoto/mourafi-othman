﻿using Microsoft.EntityFrameworkCore;



namespace ProjSVP.Models
{
    public class SVPContext : DbContext
    {

        public SVPContext(DbContextOptions<SVPContext> options)
            : base(options)
        {
        }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Service> Services { get; set; }

       
    }
}