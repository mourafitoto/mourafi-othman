﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ProjSVP.Models
{
    public class Client
    {
        [Key]
        public String login { get; set; }
        public String motDePasse { get; set; }
        public String nom { get; set; }
        public String Prenom { get; set; }
        public uint age { get; set; }
        public String adresseClient { get; set; }
    }
}