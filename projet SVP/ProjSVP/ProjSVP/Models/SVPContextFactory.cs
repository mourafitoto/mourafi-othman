﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ProjSVP.Models
{
    public class SVPContextFactory : IDesignTimeDbContextFactory<SVPContext>
    {
        SVPContext IDesignTimeDbContextFactory<SVPContext>.CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<SVPContext>();
            var connectionString = configuration.GetConnectionString("DbStr");

            builder.UseSqlServer(connectionString);

            return new SVPContext(builder.Options);
        }
    }
}